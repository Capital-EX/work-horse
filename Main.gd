extends Node2D

const Box = preload("res://entities/jobs/Box.tscn")
const BoxButton = preload("res://entities/jobs/Button.tscn")
const Computer = preload("res://entities/jobs/Computer.tscn")
const Valve = preload("res://entities/jobs/Valve.tscn")


var cash = 0
var jobs_todo = 0
var spaces = []
var choices = [0, 0, 0, 1, 1, 2, 2, 2]
var choice_index = 0
var test_point = RectangleShape2D.new()
var time = 60
var highscore = 0
onready var Player = $Objects/Player
onready var PlayerCollision = $Objects/Player/Shape
onready var player_shape = $Objects/Player/Shape.shape
onready var blink = 0
onready var blinked = false
func _ready():
	randomize()
	choices.shuffle()
	var file = File.new()
	if file.file_exists("user://high.score"):
		file.open("user://high.score", File.READ)
		highscore = file.get_32()
	$Gui/Menu.set_score(highscore)
	
	test_point.extents = Vector2(8, 8)
	$Gui/VBox/Time.text = "Time: %.1f" % time
	$Gui/VBox/Cash.text = "Cash: %d$" % cash
	spaces.resize(100)
	for y in range(0, 10):
		for x in range(0, 10):
			spaces[x + y * 10] = Vector2(x * 48 + 144, y * 48 + 144)
	$Gui/CountDown.hide()
	get_tree().paused = true

func _process(delta):
	if time > 10:
		time -= delta
	else:
		time -= delta * 0.9
	if time <= 11  and not blinked:
		$AnimationPlayer.play("Blink")
		blinked = true
	
	if time <= 0:
		get_tree().paused = true
		if cash > highscore:
			var file = File.new()
			file.open("user://high.score", File.WRITE)
			file.store_32(cash)
		$Gui/GameOverScreen/CenterContainer/Cash.text = "Cash: %d$" % cash
		$Gui/VBox.hide()
		$Gui/GameOverScreen.show()
	$Gui/VBox/Time.text = "Time: %.2f" % time
	
	
func _on_completed(job):
	$MoneyGet.pitch_scale = rand_range(0.7, 1.3)
	$Cash.emitting = false
	$Cash.emitting = true
	$Cash.position = $Objects/Player.position
	$MoneyGet.play()
	var pay = 1
	if job is BoxButtonClass:
		pay = 15
		
	elif job is ComputerClass:
		pay = 10
		
	elif job is ValveClass:
		pay = 5
	
	jobs_todo -= 1
	cash += pay
	if $JobSpawner.is_stopped():
		$JobSpawner.start()
	$Gui/VBox/Cash.text = "Cash: %d$" % cash
	spaces.push_back(job.position)

func get_position():
	var index = randi() % len(spaces)
	var pos = spaces[index]
	while player_shape.collide(PlayerCollision.global_transform, test_point, Transform2D(0, pos)):
		index = randi() % len(spaces)
		pos = spaces[index]
	spaces.remove(index)
	return pos

func _on_JobSpawner_timeout():
	if jobs_todo < 10:
		var choice = choices[choice_index]
		choice_index += 1
		if choice_index >= len(choices):
			choice_index = 0
			choices.shuffle()
		jobs_todo += 1
		if jobs_todo > 10:
			$JobSpawner.stop()
		match choice:
			0:
				var box = Box.instance()
				var button = BoxButton.instance()
				$Objects.add_child(box)
				$Objects.add_child(button)
				box.position = get_position()
				button.position = get_position()
				button.connect("completed", self, "_on_completed")
				button.connect("completed", Player, "_on_completed")
				Player.connect("move_box", box, "_on_move")
				
			1: 
				var computer = Computer.instance()
				$Objects.add_child(computer)
				computer.position = get_position()
				computer.connect("completed", self, "_on_completed")
				computer.connect("completed", Player, "_on_completed")
				Player.connect("type", computer, "_on_type")
				
			2:
				var valve = Valve.instance()
				$Objects.add_child(valve)
				valve.position = get_position()
				valve.connect("completed", self, "_on_completed")
				valve.connect("completed", Player, "_on_completed")
	

func _on_play():
	$Gui/CountDown/AnimationPlayer.play("countdown")
	$Gui/CountDown/AnimationPlayer.connect("animation_finished", self, "_on_count_donw_finished", [], CONNECT_ONESHOT)
	$ClockedIn.play()
	$ClockedOut.stop()
	$Gui/Hud.show()

func _on_count_donw_finished(animation_name):
	get_tree().paused = false
