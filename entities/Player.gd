extends KinematicBody2D
signal move_box(dir)
signal type

enum Objects {NOTHING, COMPUTER, VALVE, BOX}

const SPEED = 250
const PUSH_SPEED = 10
const ACCEL = 200000

const WALKS = {
	left = "walk-left",
	right = "walk-right",
	up = "walk-up",
	down = "walk-down",
}

const PUSHES = {
	left = "push-left",
	right = "push-right",
	up = "push-up",
	down = "push-down",
}

const IDLES = {
	left = "idle-left",
	right = "idle-right",
	up = "idle-up",
	down = "idle-down",
}


var velocity = Vector2()
var motion = Vector2()
var direction = "right"

var work = Objects.NOTHING
var touching = Objects.NOTHING
var active_object = null

onready var active_objects = {
	up = [],
	left = [],
	down = [],
	right = []
}

func _ready():
	pass

func _physics_process(delta):
	if work == Objects.NOTHING:
		walk(delta)
		
	elif work == Objects.BOX:
		push(delta)
	
	else:
		$AnimationPlayer.play(IDLES[direction])

func _on_job_completed():
	stop_working()

func push(delta):
	if Input.is_action_just_pressed("move_left"):
		motion.x -= 1
		
	if Input.is_action_just_pressed("move_right"):
		motion.x += 1
		
	if Input.is_action_just_pressed("move_up"):
		motion.y -= 1
		
	if Input.is_action_just_pressed("move_down"):
		motion.y += 1
	
	move_and_slide(motion * PUSH_SPEED)
	emit_signal("move_box", motion)
	motion.x = max(motion.x - delta * 4, 0) if motion.x > 0 else min(motion.x + delta * 4, 0)
	motion.y = max(motion.y - delta * 4, 0) if motion.y > 0 else min(motion.y + delta * 4, 0)
	
	if motion.x == 0 and motion.y == 0:
		$AnimationPlayer.stop(true)
		
	elif not $AnimationPlayer.is_playing():
		$AnimationPlayer.play(PUSHES[direction])

func walk(delta):
	motion.x = 0
	motion.y = 0
	if Input.is_action_pressed("move_left"):
		direction = "left"
		motion.x = -ACCEL * delta

	elif Input.is_action_pressed("move_right"):
		direction = "right"
		motion.x = ACCEL * delta

	if Input.is_action_pressed("move_up"):
		direction = "up"
		motion.y = -ACCEL * delta

	elif Input.is_action_pressed("move_down"):
		direction = "down"
		motion.y = ACCEL * delta

	if motion.x == 0 and motion.y == 0:
		$AnimationPlayer.play(IDLES[direction])
	else:
		if $AnimationPlayer.current_animation != WALKS[direction]:
			$AnimationPlayer.play(WALKS[direction])
	
	if motion.x == 0:
		velocity.x = max(velocity.x - ACCEL * delta, 0) if velocity.x > 0 else min(velocity.x + ACCEL * delta, 0)
	else:
		velocity.x = clamp(velocity.x + motion.x, -SPEED, SPEED)

	if motion.y == 0:
		velocity.y = max(velocity.y - ACCEL * delta, 0) if velocity.y > 0 else min(velocity.y + ACCEL * delta, 0)
	else:
		velocity.y = clamp(velocity.y + motion.y, -SPEED, SPEED)
	
	
	if motion.x != 0 or motion.y != 0:
# warning-ignore:return_value_discarded
		move_and_slide(velocity)

# warning-ignore:unused_argument
func _on_completed(object):
	stop_working()

func stop_working():
	if active_object != null:
		active_object.deactivate()
		active_object = null
		$Normal.show()
		$Pushing.hide()
		work = Objects.NOTHING
		touching = Objects.NOTHING

func _input(event):
	if event is InputEventKey and event.is_pressed() and not event.is_echo():
		if event.scancode == KEY_SPACE:
			if work != Objects.NOTHING:
				stop_working()

			elif len(active_objects[direction]) > 0:
				interact(active_objects[direction].pop_back())
				
			else:
				for dir in active_objects:
					if len(active_objects[dir]) > 0:
						interact(active_objects[dir].pop_back())
						break
		else:
			match work:
				Objects.COMPUTER:
					emit_signal("type")


func interact(object):
	if object is BoxClass:
		motion.x = 0
		motion.y = 0
		work = Objects.BOX
		$Normal.hide()
		$Pushing.show()
		active_object = object
		object.activate()

	elif object is ValveClass:
		work = Objects.VALVE
		active_object = object
		object.activate()
	
	elif object is ComputerClass:
		work = Objects.COMPUTER
		object.activate()
		active_object = object

func _on_Up_body_entered(body):
	active_objects.up.push_back(body)


func _on_Right_body_entered(body):
	active_objects.right.push_back(body)


func _on_Down_body_entered(body):
	active_objects.down.push_back(body)


func _on_Left_body_entered(body):
	active_objects.left.push_back(body)


func _on_Up_body_exited(body):
	active_objects.up.erase(body)


func _on_Right_body_exited(body):
	active_objects.right.erase(body)

func _on_Down_body_exited(body):
	active_objects.down.erase(body)


func _on_Left_body_exited(body):
	active_objects.left.erase(body)
