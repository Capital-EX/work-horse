class_name ValveClass

extends StaticBody2D
signal completed
var turns = 3
var active = false
var old_angle = 0
var delta_mouse_pos = Vector2()
var old_mouse_pos = Vector2()
var corrected_old_angle = 0
var total_rotation = 0
var start_angle = 0
onready var Valve = $Overlay/Valve
onready var TurnsLeft = $Overlay/Valve/TurnsLeft
onready var Wheel = $Overlay/Valve/Wheel


func _ready():
	TurnsLeft.text = "Turns Left: %d" % turns

func _process(delta):
	var mouse_pos = get_global_mouse_position()
	
	if active:
		var angle = Wheel.global_position.angle_to_point(get_global_mouse_position())
		Wheel.rotation = angle - PI / 2
		var corrected_angle = angle if angle >= 0 else angle + 2*PI
		var da = corrected_angle - corrected_old_angle
		if old_angle >= 0 and angle < 0:
			da = corrected_angle - (2 * PI - corrected_old_angle)
		
		elif angle >= 0 and old_angle < 0:
			da = (2 * PI - corrected_old_angle) - corrected_angle
		
		corrected_old_angle = corrected_angle
		old_angle = angle
		
		total_rotation += da
		if abs(total_rotation) > 2 * PI:
			total_rotation = 0
			turns -= 1
			TurnsLeft.text = "Turns Left: %d" % turns
			if turns == 0:
				emit_signal("completed", self)
				queue_free()
	
	if not active and Input.is_key_pressed(KEY_M):
		activate()
	

func activate():
	old_angle = Wheel.global_position.angle_to_point(get_global_mouse_position())
	corrected_old_angle = old_angle if old_angle >= 0 else old_angle + 2*PI
	total_rotation = 0
	active = true
	Valve.show()
	
func deactivate():
	active = false
	Valve.hide()

