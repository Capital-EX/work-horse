extends Button

func _on_pressed():
	InputMap.get_action_list("move_up")[0].scancode = KEY_W
	InputMap.get_action_list("move_right")[0].scancode = KEY_D
	InputMap.get_action_list("move_down")[0].scancode = KEY_S
	InputMap.get_action_list("move_left")[0].scancode = KEY_A
	
	$"../UpSettings/UpKeyBinding".text = "W"
	$"../RightSettings/RightKeyBinding".text = "D"
	$"../DownSettings/DownKeyBinding".text = "S"
	$"../LeftSettings/LeftKeyBinding".text = "A"
