extends HSlider
const format_string = "%d%%"
const LOG10 = log(10)
const log_10_100 = log(100.0) / LOG10
const eighty_thrids = 80 / 3
func log10(x):
	return log(x) / LOG10

onready var label = $"../Percent"

func _ready():
	pass


func _on_VolumeSlider_value_changed(value):
	var log_value = log10(1 / value) * eighty_thrids
	label.text = format_string % (value * 100)
	var master_bus = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_volume_db(master_bus, -log_value)
