extends CenterContainer
signal play

func _ready():
	$PanelContainer/Settings/ControlSettings/Confirm.connect("pressed", self, "_on_Confirm_pressed")
	$PanelContainer/HowToPlay/VBoxContainer2/Close.connect("pressed", self, "_on_close_pressed")

	
	

func set_score(score):
	$PanelContainer/VBoxContainer/Title/HighScore.text = "High Score: %d$" % score

func _on_Button_pressed():
	hide()
	emit_signal("play")


func _on_Settings_pressed():
	$PanelContainer/VBoxContainer.hide()
	$PanelContainer/Settings.show()

func _on_Confirm_pressed():
	$PanelContainer/VBoxContainer.show()
	$PanelContainer/Settings.hide()


func _on_HowToPlay_pressed():
	$PanelContainer/VBoxContainer.hide()
	$PanelContainer/HowToPlay.show()
	
func _on_close_pressed():
	$PanelContainer/HowToPlay.hide()
	$PanelContainer/VBoxContainer.show()
