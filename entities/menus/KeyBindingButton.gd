extends Button
export (String) var event_name
var active = false
onready var input = InputMap.get_action_list(event_name)[0]

func _ready():
	connect("toggled", self, "_on_toggled")
	text = OS.get_scancode_string(input.scancode)


func _input(event):
	if active:
		if event is InputEventKey:
			if event.scancode == KEY_ESCAPE:
				pressed = false
			else:
				input.scancode = event.scancode
				pressed = false

func _on_toggled(button_pressed):
	active = button_pressed
	if active:
		text = "..."
	else:
		text = OS.get_scancode_string(input.scancode)
